# CV Mikhail Iost

![](assets/iost.jpeg "Mikhail Iost")

## Position

Golang-/PHP-/Web-developer

## Contact information

- :round_pushpin: Minsk, Belarus
- :telephone: [+375-25-728-1-666](tel:+375-25-728-1-666)
- :email: [mikhail.iost@gmail.com](mailto:mikhail.iost@gmail.com)
- [skype](skype:mikhail.iost)
- [telegram](https://t.me/indikator)
- [gitlab](https://gitlab.com/indikator)
- [linkedin](https://linkedin.com/in/mikhail-iost)

## Working experience

| Dates | Position description |
| ---   | ---                  |
| 2018-06-01 - Present | MB "Piksis", Lithuania (remotely). Web-developer. Projects stacks: Golang - buffalo, gorm. PHP - Symfony. DB - MySQL, PostgreSQL. There is a few projects on longterm support: 1. [cikuraku.lt](cikuraku.lt) - this is a CRM based on Simfony 2.4 and MySQL as a DB. Typical task is - adding new feature, integrating with external services like a post.lt., automatization existing features 2. [balticblues.com](balticblues.com) - there is CRM written in pure PHP. DB - MySQL. Typical task here - adding new features, optimizing DB queries. Also there was my first step in Golang production - I've written a service for parsing "rooming list" (multiformatting text files parsed by Apache Tika and after that Regex prepares data in strong structure and return it to CRM. 3. We've started writing a core for CRM system based on Buffalo (Golang framework) + GORM + PostgreSQL (Selected it for 2 features - JSONB and table inheritance...but failed twice - for normal working with JSON we have to write a lot of RAW SQL, PostgreSQL don't allow to make foreign key to parent table)|
| 2021-03-01 - Present | ManCanWeb, Lithuania (remotely). Backend-developer. Projects stacks: Golang - buffalo, gorm. PHP - Laravel. DB - MySQL, PostgreSQL. There is a main project - [wecreators.com](wecreators.com). We develop this system from scratch. Laravel for backend API (REST), MySQL as a DB - this is analogue of Upwork for University internal use. Satellite project linked with core for CRM (see above)|
| 2020-03-01 - 2021-07-15 | Shop.by, Belarus (remotely). Backend-developer. Projects stack: Golang - graphql, xorm. PHP - Yii1, Yii2, Laravel. DB - MySQL. Highload. This is a shop aggregator - one of most known in Belarus. Has been written long time ago on C++ (core) + XSLT templates. I've rewrittencatalog models and catalog types to Golang. There were API based on GraphQL and xorm as DB ORM. RoadRunner used as a router + worker for PHP (Spiral). PHP selected for using Twig templates (not my area)|
| 2015-02-01 - 2019-06-01 | Visutech System, Belarus. Web-developer. Projects stack: PHP, MySQL. Main project was CRM based on vTiger CRM + Customer Portal (PHP). vTiger CRM was an SOAP API for customer portal. DB - MySQL. Typical task - implementation new features for Sales Managers.|

## English level

Intermediate

## Salary expectations

5000 euro gross -> net (+- 15%)
